#!/bin/bash -e

# Create terminalserver network in case it doesn't exist
docker network inspect terminalserver_default 1>/dev/null \
  || docker network create terminalserver_default

# Get query for DB creation
mkdir -p ./data/docker-entrypoint-initdb.d
docker compose run --rm --no-deps guacamole /opt/guacamole/bin/initdb.sh --postgresql > ./data/docker-entrypoint-initdb.d/10-initdb.sql

# Update default password for guacadmin
source .env
cat << EOF >> ./data/docker-entrypoint-initdb.d/10-initdb.sql 
UPDATE guacamole_user
  SET  password_salt = NULL, password_hash = SHA256('$GUACADMIN_PASSWORD')
     WHERE user_id = 1;
EOF


# Start database 
docker compose up -d guacamole-db 

# Print tables
docker exec guacamole-db sh -c 'until pg_isready; do sleep 3; done; psql --username=guacamole --dbname=guacamole --command="\dt"'
