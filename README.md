0. `cp .env.template .env` and update your hostname, portmapping and guacadmin password in `.env`


1. Populate database PRIOR to first startup:
Populate .env from template, set the guacadmin *password*, *hostname* and *populate database*:
```
docker compose pull
cp .env.template .env && sed -i -e "s/ReplaceWithYourSecretPassword/`pwgen  -B 20 1`/" .env
./initdb.sh && docker compose up -d
```

2. Browse http://localhost:8080/guacamole/
   and login with login `guacadmin` and password set in .env 

3. Settings - Connections
   (All no described field can stay blank.)
   - RDP (xrdp)
     - Name = some freetext
     - Protocol = RDP
     - Parameters
       - Hostname or IP
       - Port =  3389
       - Username
       - Password
       - Security mode: Any 
   - RDP (Windows 10)
     - Name = some freetext
     - Protocol = RDP
     - Parameters
       - Hostname or IP
       - Port =  3389
       - Username
       - Password
       - Security mode = TLS
       - Disable authentication = [x]
       - Ignore server certificate = [x] 
   - SSH
     - Name = some freetext
     - Protocol = SSH
     - Parameters
       - Hostname or IP
       - Port = 22 
       - Username
       - Password
       - Security mode: Any
     - SFTP
       - Enable SFTP

4. Accessing other containers 
Example:
```
  guacd:
#   ...
    networks:
      - default
      - terminalserver_default

networks:
  terminalserver_default:
    external: true
```
